package de.summerfeeling.labytwitter;

import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.SettingsElement;

import java.util.List;

public class LabyTwitter extends LabyModAddon {
	
	public void onEnable() {
		TwitterModule module = new TwitterModule();
		
		this.api.registerForgeListener(module);
		this.api.registerModule(module);
	}
	
	public void onDisable() {
	
	}
	
	public void loadConfig() {
	
	}
	
	protected void fillSettings(List<SettingsElement> list) {
	
	}
}
